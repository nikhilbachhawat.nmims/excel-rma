export const LIST_SALES_INVOICE_ENDPOINT = 'api/sales_invoice/v1/list';
export const DIRECT_PROFILE_ENDPOINT = '/api/direct/v1/profile';
export const SALES_INVOICE_GET_ONE_ENDPOINT = '/api/sales_invoice/v1/get/';
export const LIST_ITEMS_ENDPOINT = 'api/item/v1/list';
export const CREATE_SALES_INVOICE_ENDPOINT = 'api/sales_invoice/v1/create';
export const UPDATE_SALES_INVOICE_ENDPOINT = 'api/sales_invoice/v1/update';
export const SUBMIT_SALES_INVOICE_ENDPOINT = 'api/sales_invoice/v1/submit';
export const RELAY_LIST_COMPANIES_ENDPOINT =
  '/api/settings/v1/relay_list_companies';
export const GET_SETTINGS_ENDPOINT = '/api/settings/v1/get';
export const UPDATE_SETTINGS_ENDPOINT = '/api/settings/v1/update';
export const LIST_CUSTOMER_ENDPOINT = 'api/customer/v1/list';
export const LIST_SERIAL_ENDPOINT = 'api/serial_no/v1/list';
export const LIST_WAREHOUSE_ENDPOINT =
  'api/delivery_note/v1/relay_list_warehouses';
export const GET_USER_PROFILE_ROLES = '/api/settings/v1/profile';
export const ASSIGN_SERIAL_ENDPOINT = '/api/serial_no/v1/assign';
export const VIEW_SALES_INVOICE_PAGE_URL = 'view-sales-invoice';
export const ADD_SALES_INVOICE_PAGE_URL = 'add-sales-invoice';
export const RELAY_LIST_PRICELIST_ENDPOINT =
  '/api/command/user/api/resource/Price%20List';
export const RELAY_GET_ITEMPRICE_ENDPOINT =
  '/api/command/user/api/resource/Item Price';
export const LIST_TERRITORIES_ENDPOINT = '/api/territory/v1/list';
export const RELAY_LIST_TERRITORIES_ENDPOINT =
  '/api/command/user/api/resource/Territory';
export const CREATE_TERRITORY_ENDPOINT = '/api/territory/v1/create';
export const UPDATE_TERRITORY_ENDPOINT = '/api/territory/v1/update';
export const DELETE_TERRITORY_ENDPOINT = '/api/territory/v1/remove';
