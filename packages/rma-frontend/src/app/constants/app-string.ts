export const CLOSE = 'Close';
export const SHORT_DURATION = 5000;
export const UPDATE_SUCCESSFUL = 'Update Successful';
export const UPDATE_ERROR = 'Update Error';
export const SYSTEM_MANAGER = 'System Manager';
